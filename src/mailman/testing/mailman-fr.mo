��          �   %   �      `  C   a     �     �     �  /   �          "     0     J     R  (   r     �     �  C   �  #    ^  4     �  &   �     �  =   �  F   #  u  j  R   �  %   3	     Y	     g	    u	  F   �
     �
     �
     
  9   %  "   _     �     �     �  6   �  L   �  +   E  !   q  `   �  U  �  �  J  %   �  '   !     I  I   \  J   �  �  �  k   �  )     "   :     ]                                                                        
                                  	                  ${mlist.display_name} Digest, Vol ${volume}, Issue ${digest_number} An alias for 'end'. An alias for 'join'. An alias for 'leave'. Confirm a subscription or held message request. Digest Footer Digest Header Echo back your arguments. End of  Get a list of the list members. Get help about available email commands. Join this mailing list. Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Send $display_name mailing list submissions to
	$got_list_email

To subscribe or unsubscribe via email, send a message with subject or body
'help' to
	$got_request_email

You can reach the person managing the list at
	$got_owner_email

When replying, please edit your Subject line so it is more specific than
"Re: Contents of $display_name digest..." Stop processing commands. Today's Topics (%(msgcount)d messages) Today's Topics:
 Welcome to the "${mlist.display_name}" mailing list${digmode} You have been unsubscribed from the ${mlist.display_name} mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. [Message discarded by content filter] digest footer digest header Project-Id-Version: mailman
PO-Revision-Date: 2006-11-14 12:07+0100
Last-Translator: Frederic Daniel Luc Lehobey
Language-Team: mailman-fr <mailman-fr@rezo.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.3
 Groupe ${mlist.display_name}, Vol ${volume}, Parution ${digest_number} Un alias pour « end ». Un alias pour « join ». Un alias pour « leave ». Confirmer un abonnement ou une demande de message retenu. Pied de page des remises groupées En-tête des remises groupées Répète vos arguments. Fin de  Récupérer une liste de tous les membres de la liste. Obtenez de l’aide sur les commandes de courrier électronique disponibles. Inscrivez-vous à cette liste de diffusion. Quitter cette liste de diffusion. Quitter cette liste de diffusion.

Il vous sera peut-être demandé de confirmer votre requête. Produit une liste de noms d'abonnés et d'adresses de courriel.


Les paramètre optionnels delivery= et mode= peuvent être utilisés pour limiter le rapport
aux abonnés qui correspondent au statut et/ou au mode de distribution. Si soit delivery= ou mode=
est spécifié plus d'une fois, seule la dernière occurrence est prise en compte.
 Envoyez vos messages pour la liste $display_name à
	$got_list_email

Pour vous (dés)abonner par courriel, envoyez un message avec « help » dans
le corps ou dans le sujet à
	$got_request_email

Vous pouvez contacter l'administrateur de la liste à l'adresse
	$got_owner_email

Si vous répondez, n'oubliez pas de changer l'objet du message afin
qu'il soit plus spécifique que « Re: Contenu du groupe de $display_name... » Arrête d'interpréter les commandes. Thèmes du jour (%(msgcount)d messages) Thèmes du jour :
 Bienvenue sur la liste de diffusion${digmode} « ${mlist.display_name} » Vous avez été désabonné de la liste de diffusion ${mlist.display_name} Il vous sera demandé de confirmer votre demande d'abonnement et vous pourrez
avoir reçu un mot de passe provisoire.

En utilisant l'option « digest », vous pouvez spécifier si vous désirez la distribution de
condensés ou non. Si absente, le mode de distribution par défaut de la liste sera utilisé.
Vous pouvez utiliser l'option « address » pour demander l'inscription d'une adresse
autre que celle effectuant la commande.
 Votre confirmation est nécessaire pour vous abonner à la liste de diffusion ${event.mlist.fqdn_listname}. [Message rejeté par filtrage de contenu] pied de page des remises groupées en-tête des remises groupées 